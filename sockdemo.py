#!/usr/bin/python

import socket
import sys
import time 
import json

# Create a UDP socket
sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
# Connect the socket to the port where the server is listening
server_address = '/tmp/SOCKET'
print >>sys.stderr, 'connecting to %s' % server_address

#try:
#    sock.connect(server_address)
#except socket.error, msg:
#    print >>sys.stderr, msg
#    sys.exit(1)


class UDSConnection:
    def __init__(self, socketname):
        self.socketname = socketname
        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)       
        try:
            self.socket.connect(socketname)
            self.socket.settimeout(4.0)
        except socket.error, msg:
            print >>sys.stderr, msg
            sys.exit(1)

    def sendMsg(self, message):        
        #sent = sock.sendto("\x1e" + message + "\x0a", server_address)
        sent = self.socket.sendall("\x1e" + message + "\x0a")

    def readMsg(self):
        message = None
        data = self.socket.recv(1024)
        start = data.find("\x1e")
        end = data.find("\x0a")
        if start > -1 and end > -1:
            message = data
            # Todo: handle case when end = -1 (no end-eperator read from socket)
        print("Start:%u, end:%u" % (start,end))
        print(message)

    def close(self):
        self.socket.close()

conn = UDSConnection('/tmp/SOCKET')
obj = {}
obj['action'] = "sendReport"
obj['data'] = "bladderpadder";
conn.sendMsg(json.dumps(obj))

conn.readMsg()

#message = 'This is the message.  It will be repeated.'
#try:
    # Send data
    #print >>sys.stderr, 'sending "%s"' % message
    #sent = sock.sendto("\x1e" + message + "\x0a" + "\x1e" + "The next message", server_address)
    #time.sleep(5)
    #sent = sock.sendto("-end of next message\x0a", server_address)
    # Receive response
    ##print >>sys.stderr, 'waiting to receive'
    #data, server = sock.recvfrom(4096)
    #print >>sys.stderr, 'received "%s"' % data

#finally:
#    print >>sys.stderr, 'closing socket'
 #   sock.close()
