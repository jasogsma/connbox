var express = require('express');
var app = express();
var WebSocketServer = require('ws').Server
var wss = new WebSocketServer({ port: 40510 })
var PosixMQ = require('posix-mq')

var bodyParser = require('body-parser')
app.use(bodyParser.json());

// Pre-exit scripts
let preExit = [];

// Catch exit
process.stdin.resume();
process.on ('exit', code => {
  let i;

  console.log ('Process exit');

  for (i = 0; i < preExit.length; i++) {
    preExit[i] (code);
  }

  process.exit (code);
});

// Catch CTRL+C
process.on ('SIGINT', () => {
  console.log ('\nCTRL+C...');
  process.exit (0);
});

// Catch uncaught exception
process.on ('uncaughtException', err => {
  console.dir (err, { depth: null });
  process.exit (1);
});


// ******* Application code ************

//app.use(express.json());       // to support JSON-encoded bodies
//app.use(express.urlencoded()); // to support URL-encoded bodies

app.get('/API/text', function (req,res) {
	console.log('GET for API/text')    
    res.json({"foo": "bar"});
    })

app.post('/API/text', function (req,res) {
    console.log('POST for API/text')
    console.log(req.body)
    res.json({"status": "OK"});
    })

    
app.get('/API/status', function (req,res) {
    data = {}
    data.status = "operational"
    data.drivers = ["Simulated","MiniC"]
    miniC = {}
    miniC.status = "logged in"
    miniC.SNR = 50.3
    data.MiniC = miniC

    res.json(data);
})

var out_message = ""

app.post('/trigger/text', function (req, res) {
    console.log(req.body)
    res.json({ "status": "OK" })
    wss.clients.forEach(function each(ws) {
        ws.send(JSON.stringify(req.body))
    })
})

// **** WS Stuff ****
function noop() {}

function heartbeat() {
  this.isAlive = true;
}

wss.on('connection', function connection(ws) {
    ws.isAlive = true;
    ws.on('pong', heartbeat);
});

const interval = setInterval(function ping() {
    console.log("WSS housekeeping")
    wss.clients.forEach(function each(ws) {
        console.log("checking ws " + ws)
      if (ws.isAlive === false) return ws.terminate();
  
      ws.isAlive = false;
      ws.ping(noop);
    });
  }, 30000);

  
// MQ stuff
var mq = new PosixMQ();
mq.on('messages', function() {
    var n;
    while ((n = this.shift(readbuf)) !== false) {
	console.log("Received message ("+ n +" bytes): "+ readbuf.toString('utf8', 0, n));
	console.log("Messages left: "+ this.curmsgs);
    }
   // this.close();
});
mq.open({name: '/pmqtest'});
readbuf = new Buffer(mq.msgsize);

app.use(express.static('./'))
app.listen(3000, () => console.log('Server listening on port 3000!'))

preExit.push (code => {
    console.log ('Exit code %d', code);
  });