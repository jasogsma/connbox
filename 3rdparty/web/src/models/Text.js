
var m = require("mithril")

var Text = {
    values: {},
    sendText: function () {
        console.log("Posting text to server")
        console.log(Text.values)
        return m.request({
            method: "POST",
            url: "/API/text",       
            data: Text.values
        })
            .then(function (result) {
                console.log(result)
                //this.values = result
            })
    },
}

module.exports = Text
