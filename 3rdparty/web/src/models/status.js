
var m = require("mithril")
var Status = {
    values: {},
    miniC: {},
    loadStatus: function () {
        return m.request({
            method: "GET",
            url: "/API/status",
            withCredentials: false
        })
            .then(function (result) {
                Status.values = result
                console.log(Status.values)
                Status.miniC = result.MiniC
            })
    },
}

module.exports = Status
