var m = require("mithril")

var Receiver = {
    text: "N/A",
    connect: function() {
	var wstarget = 'ws://' +  location.hostname + ':40510'
        console.log('Connecting to ' + wstarget)
//        var ws = new WebSocket('ws://tinkerboard.local:40510');
	var ws = new WebSocket(wstarget)

        ws.onopen = function () {
            console.log('websocket is connected ...')
            ws.send('connected')
        }
        
        ws.onmessage = function (ev) {            
            var obj = JSON.parse(ev.data);
            //console.log(obj['message'])
           // Receiver.textReceived(obj['message'])
           
           Receiver.text = obj['message']
           console.log(Receiver.text)
           m.redraw()
        }
    },
   // textReceived: function (text) {
   //     console.log("Received:" + text)
   //     Receiver.text = text
   // }
}

module.exports = Receiver


var ws = new WebSocket('ws://localhost:40510');
//var Receiver = require("../models/Receiver")

