var m = require("mithril")
var Status = require("../models/status")

var StatusComponent = {    
    oninit: function(vnode) {
        Status.loadStatus()
    },
    view: function (vnode) {   
        var drivers = []
        if (Object.keys(Status.values).length > 0) {
               drivers = Status.values['drivers']
        }
        return (            
            <div>
            <ul>
                <li>Status: {Status.values['status']}</li>
                <li>Drivers: <ul>{ 
                    drivers.map((driver) => (
                        <li>{driver}</li>
                    ))
                }</ul></li>
                
            </ul>
            <div class="text-status">Status:{Status.miniC['status']}, SNR: {Status.miniC['SNR']}</div>
            </div>
        )
    }
}
module.exports = StatusComponent
