var m = require("mithril")
var Receiver = require("../models/Receiver")

var ReceiverComponent = {
    oninit: function(vnode) {
        Receiver.connect()
    },    
    view: function (vnode) {      
        return (
            <div>
            Received text:<br/>
            <textarea cols="80" rows="5">
             {Receiver.text}
             </textarea>
             Slut
             </div>
        )
    }
}

module.exports = ReceiverComponent
