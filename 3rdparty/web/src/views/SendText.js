var m = require("mithril")
var Text = require("../models/Text")

var SendText = {
    send: function () {        
        Text.sendText
    },
    view: function (vnode) {
        return m("form", {
            onsubmit: function (e) {
                e.preventDefault()                
                Text.sendText()
            }
        }, [
                m("textarea.textarea[cols=80][rows=5][placeholder=Message to send]",
                 {oninput: m.withAttr("value", function(value) {
                     Text.values.messageText = value
                    }), value: Text.values.messageText}),    
                    m("br"),             
                m("button.button[type=submit]", "Send"),
            ])
    }
}

module.exports = SendText
