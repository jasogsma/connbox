// src/index.js
var m = require("mithril")

var UserList = require("./views/UserList")
var SendText = require("./views/SendText")
var StatusComponent = require("./views/Status")
var ReceiverComponent = require("./views/Receiver")

m.mount(document.body, {
    view: function(vnode) {
        return [
                m("h1", "ConnectionBox NG+"),
                m(StatusComponent),
                m(SendText),
                m(ReceiverComponent)
        ]
    }
})