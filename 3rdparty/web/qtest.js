var PosixMQ = require('posix-mq')

var mq_out = new PosixMQ();
//mq_out.on('messages', function() {
//    var n;
//    var readbuf = new Buffer(mq_out.msgsize);
//    while ((n = this.shift(readbuf)) !== false) {
//	console.log("Received message ("+ n +" bytes): "+ readbuf.toString('utf8', 0, n));
//	console.log("Messages left: "+ this.curmsgs);
//    }
//});


mq_out.open(    
        {
            name: '/terminal'
        }
    )    


var in_queue_name = '/frontend-' + process.pid
var mq_in = new PosixMQ();

mq_in.on('messages', function() {
    var n;
    var readbuf = new Buffer(mq_in.msgsize);
    while ((n = this.shift(readbuf)) !== false) {
	console.log("Received message ("+ n +" bytes): "+ readbuf.toString('utf8', 0, n));
	console.log("Messages left: "+ this.curmsgs);
    }    
});


mq_in.open({
    name: in_queue_name,
    create: true,
    mode: 0600,
    maxmsgs: 10,
    msgsize: 1024
});


//writebuf = new Buffer(mq.msgsize);
var request = {"action":"status", "type":"system", "source": in_queue_name}
mq_out.push(JSON.stringify(request))



