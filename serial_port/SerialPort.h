#ifndef __SERIAL_PORT_H
#define __SERIAL_PORT_H

#include <string>

/**
 * Serial port class
 * See https://www.cmrr.umn.edu/~strupp/serial.html#3_1.
 * 
 */

class SerialPort
{
  public:
    SerialPort(std::string device);
    ~SerialPort();
    bool openPort(void);
    void closePort(void);
    int send(const char *str);
    int receive(const char *delimiter, char **str);
    bool hasPrompt();
    void purgeBuffer();
    bool isOpen();
private:
    bool opened = false;
    int _fd;
    char rx_buffer[100 * 1024]; //  @todo: Get around this fixed limit   
    char line_buffer[100 * 1024];
    int read_index = 0;
};

#endif // __SERIAL_PORT_H
