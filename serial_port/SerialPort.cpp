#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <string.h>
#include <cstring>
#include <termios.h>
#include <unistd.h>

#include "SerialPort.h"

using namespace std;
#define TRACE(ARG) cout << #ARG << endl;

SerialPort::SerialPort(std::string device)
{
    cout << "Opening " << device << endl;
    _fd = open(device.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    if (_fd == -1)
    {
        perror("SerialPort: open(): Unable to open device");
    }
    else
    {
        opened = true;
        fcntl(_fd, F_SETFL, 0);
        struct termios options;
        tcgetattr(_fd, &options);
        
        /* TODO: SEt up to match:
         * speed 115200 baud; line = 0;
            min = 0; time = 10;
            -icrnl -imaxbel
            -opost -onlcr
            -isig -icanon -echo -echoe
         */
        cfsetispeed(&options, B115200);
        cfsetospeed(&options, B115200);

        options.c_cflag |= (CLOCAL | CREAD);
        /* Raw input */
        options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
        /* Raw output */
        options.c_oflag &= ~OPOST;

        /* 8N1 */
        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS8;

        /* Timeout settings */
        options.c_cc[VMIN]  = 0;
        options.c_cc[VTIME] = 10;
        
        tcsetattr(_fd, TCSANOW, &options);
    }
    read_index = 0;
}

SerialPort::~SerialPort() {
  closePort();    
}

bool SerialPort::openPort(void)
{
    return 0;
}

void SerialPort::closePort(void)
{
    close(_fd);
    _fd = -1;
}

int SerialPort::send(const char *str)
{
    ssize_t cnt = 0;
    if ( (str != NULL) && (_fd > -1) )  {
        cnt = write(_fd, str, strlen(str));
    } 
    if(cnt<0){
        _fd = -1;
    }
    return cnt;
}

int SerialPort::receive(const char *delimiter, char **str) {
    int res = -1;

    if (_fd <= -1) {
        return -1;
    }
    
    ssize_t cnt = 0;    
    int capacity = sizeof (rx_buffer) - read_index - 1;
    
    do {
        cnt = read(_fd, rx_buffer + read_index, capacity);
        if (cnt > 0) {
            read_index += cnt;
            rx_buffer[read_index] = 0;

            res = cnt;

            // See if there is a whole line as specified by <delimiter>, return if so
            if (delimiter != NULL) {

                char *p = strstr(rx_buffer, delimiter);
                if (p != NULL) {
                    res = 0;
                    int len = p - &rx_buffer[0];
                    memcpy(line_buffer, &rx_buffer[0], len);
                    line_buffer[len] = 0;
                    *str = line_buffer;
                    memcpy(&rx_buffer[0], p + strlen(delimiter), read_index - len);
                    read_index = read_index - len - strlen(delimiter);
                    return res;
                }
            }
        } else  {          
            return -3;
        }
        capacity = sizeof (rx_buffer) - read_index;
    } while ((cnt > 0) && (capacity > 0));
    
    if (capacity < 1) {
        printf("Buffer full! - read_index = %i\n", read_index);
        res = -2;
    }

    return res;
}


bool SerialPort::hasPrompt()
{
    return (strcmp(rx_buffer,": ") == 0);
}

void SerialPort::purgeBuffer()
{
  read_index = 0;  
}

bool SerialPort::isOpen() {
    return opened;
}