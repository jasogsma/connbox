/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MiniC.h
 * Author: stephenaaskov
 *
 * Created on 23. januar 2018, 23:17
 */

#ifndef MINIC_H
#define MINIC_H
#include <string>
#include <map>
#include <vector>
#include <memory>
#include <list>
#include "SerialPort.h"
#include "TxLogEntry.h"
#include "RxLogEntry.h"
#include "EGCListEntry.h"

using namespace std;

class MiniC {
public:
    MiniC(const char *device);    
    MiniC(const MiniC& orig) = delete;
    virtual ~MiniC();
    bool open();
    void cleanInput();

    bool isConnected() const;
    
    std::map<std::string,std::string> device_info;
    std::map<std::string,std::string> hardware_status;
    
    std::map<std::string, std::string> *hardwareStatus();
    std::map<std::string,std::string> *status();
    bool sendBytes(string filename, const char *buffer, size_t cnt);
    
    std::unique_ptr<vector<char>> readfile(string filename);
    std::unique_ptr<std::map<unsigned int, std::shared_ptr<EGCListEntry>>> listEGC();    
    std::unique_ptr<std::vector<std::shared_ptr<TxLogEntry>>> TXLog();
    std::unique_ptr<std::vector<std::shared_ptr<RxLogEntry>>> RXLog();
    std::unique_ptr<std::list<std::string>>commandLog();
    
    std::string getNextTxFilename();
    void doCommand(std::string cmd);
    bool isTcu() const;
    void setTcu(bool tcu);
private:
    bool stringToMap(char *str, std::map<std::string,std::string> *map);
    bool connected = false;
    bool tcu = false; // The connected device is a TCU
    std::string m_device;
    SerialPort *m_port;
    unsigned int m_TxSeq = 42;
};

#endif /* MINIC_H */

