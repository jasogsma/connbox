/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MiniC.cpp
 * Author: stephenaaskov
 * 
 * Created on 23. januar 2018, 23:17
 */

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <stdio.h>
#include "MiniC.h"
#include <iostream>
#include <sstream>
#include <regex.h>
#include <map>
#include <list>
#include <string>
#include <algorithm>
#include <functional>

#include "TxLogEntry.h"
#include "EGCListEntry.h"

using namespace std;

void RemoveSpaces(char* source)
{
    unsigned int upper = 0;
  char* i = source;
  char* j = source;
  while(*j != 0)
  {
    *i = *j++;
    if (upper) {
        upper = 0;
        *(i) = toupper(*(i));
    }
    if(*i != ' ') {
      i++;
    } else {
      upper = 1;
    }
  }
  *i = 0;
}

MiniC::MiniC(const char *device) {
    m_device = device;
    m_port = new SerialPort(m_device.c_str());
}

void MiniC::cleanInput() {
    if (!m_port) {
        return;
    }

    m_port->send("\r");
    char *str = NULL;
    while (m_port->receive(":", &str) == 0) {
        //std::cout << "Cleaner: '" << str << "'" << std::endl;
        char *p = str;
        for (int i=0;i< strlen(str);i++) {
            printf("0x%x ", *p);
            p++;
        }
        std::cout << std::endl;
    }
}

std::string MiniC::getNextTxFilename() {
    ostringstream ss;
    ss << "OUTPUT." << m_TxSeq++;
    return ss.str();
}

bool MiniC::stringToMap(char *str, std::map<std::string, std::string> *map) {
    char *key = strtok(str, ":");
    char *value = NULL;
    if (key != NULL) {
        while (key && isspace(*key)) {
            key++;
        }

        char *end = key + strlen(key) - 1;
        while (isspace(*end) && (end >= str)) {
            *end = 0;
            end--;
        }

        value = strtok(NULL, ":");
        while (value && !isalnum(*value) && *value != '-' && *value != '\0') {
            value++;
        }
        if (value && *value != '\0') {
            end = value + strlen(value) - 1;
            *end = 0;            
            char *p = value;
            char *last = p;
            while (p && *p != '\0') {
                p++;
                if (*p == ' ' && *last == ' ') {
                    *last = '\0';
                    break;
                }
                last = p;
            }
                       
        }
    }
    if (key && value) {
        RemoveSpaces(key);
        map->insert(std::pair<string, string>(key, value));
    }
}


#if 0
bool MiniC::hardwareStatus() {
    if (!m_port) {
        return false;
    }
    std::cout << "hardwareStatus()" << std::endl;
    
    m_port->send("\r\n");
    char *str = NULL;
      while (m_port->receive("\r\n", &str) == 0) {
    }
    
    m_port->send("st -w\r\n");    
    while (m_port->receive("\r\n", &str) == 0) {
            std::cout << str << std::endl;
        if (strlen(str)) {
            std::cout << str << std::endl;
        }
    }
}
#endif

std::map<std::string, std::string> *MiniC::hardwareStatus() {
    if (!m_port) {
        return NULL;
    }
    cleanInput();
    m_port->purgeBuffer();
    std::map<std::string, std::string> *values = new std::map<std::string, std::string> ();

    char *str = NULL;
    m_port->send("st -w\r\n");
    str = NULL;
    hardware_status.clear();
        
    while (m_port->receive("\r\n", &str) == 0) {
        stringToMap(str, &hardware_status);
    }
    std::cout << "Done reading from terminal" << std::endl;
    if (device_info.size() > 0) {
        connected = true;
    }
    // showing contents:
    std::cout << "hardware_status contains:\n";
    for (auto& kv : hardware_status) {
        std::cout << kv.first << "=>" << kv.second << '\n';
    }
    return values;
}

std::unique_ptr<std::map<unsigned int, std::shared_ptr<EGCListEntry>>> MiniC::listEGC() {
    auto log = std::make_unique<std::map<unsigned int, std::shared_ptr<EGCListEntry>>>();
    if (m_port) {

        m_port->send("\r");
        char *str = NULL;
        while (m_port->receive("\r", &str) == 0) {
        }

        std::map<unsigned int, std::string> EGCList;
        m_port->send("ls EGC.*\r");
        while (m_port->receive("\r", &str) == 0) {
            if (strlen(str)) {
                char *p = str;
                while (*p && (*p == '\n' || *p == '\r')) {
                    p++;
                }

                if (p && strlen(p) && *p != ' ') {
                    //EGCListEntry *entry = new EGCListEntry(p);
                    auto entry = std::make_shared<EGCListEntry>(p);
                    //std::cout << "EGC Entry:" << entry->GetNumber() << std::endl;
                    if (entry->GetNumber() > 0) {
                        log->insert(std::make_pair(entry->GetNumber(), entry));
                    } 
                }
            }
        }
    }
    return log;
}

std::unique_ptr<std::vector<std::shared_ptr<TxLogEntry>>> MiniC::TXLog() {
    auto log = std::make_unique<std::vector<std::shared_ptr<TxLogEntry>>>();
    if (m_port) {
        m_port->send("\r");
        char *str = NULL;
        while (m_port->receive("\r", &str) == 0) {
        }

        m_port->send("st -t\r");
        int state = 0;
        while (m_port->receive("\r", &str) == 0) {
            if (strlen(str)>2) {
                char *p = str;
                while (*p && (*p == '\n' || *p == '\r')) {
                    p++;
                }
                
                if (*p == '-') {
                    while (*p != '\0' && *p == '-') {
                        p++;
                    }
                    //std::cout << "P1: '" << p << "'" << std::flush << std::endl;
                    while (*p != '\0' && (*p == '\n' || *p == '\r')) {                       
                        p++;
                    }
                    state = 1;
                }                
                
                if (strlen(p) > 0 && state > 0 && *p != ' ') {                                       
                    char *p1 = strtok(p, " ");
                    char *parts[11];
                    int i = 0;
                    while (p1 && i<11) {
                        parts[i++] = p1;                        
                        p1 = strtok(NULL," ");
                    }                    
                    auto entry = std::make_shared<TxLogEntry>(parts);                                     
                    log->push_back(entry);
                }               
            }
        }
    }
    
    std::sort(log->begin(), log->end(), []( const auto &lhs,  const auto &rhs) {
        return lhs->GetDate() > rhs->GetDate();
    });
    
    return log;
}


/* TODO: refactor RXLog and TXLog to use same code to read from terminal */
std::unique_ptr<std::vector<std::shared_ptr<RxLogEntry>>> MiniC::RXLog() {
/*
    Comparator compFunctor =
            [](std::pair<std::string, int> elem1, std::pair<std::string, RxLogEntry> elem2) {
                return elem1.second < elem2.second;
            };
            */
    
    /*
    auto cmpLambda = [](const string &lhs, const RxLogEntry &rhs) {
        return true;//lhs.y < rhs.y;
    };
    */
    // [](const string &lhs, const RxLogEntry &rhs) {  return true;}
    
    auto log = std::make_unique<std::vector<std::shared_ptr<RxLogEntry>>>();
    if (m_port) {
        m_port->send("\r");
        char *str = NULL;
        while (m_port->receive("\r", &str) == 0) {
        }

        m_port->send("st -r\r");
        int state = 0;
        while (m_port->receive("\r", &str) == 0) {
            if (strlen(str)>2) {
                char *p = str;
                while (*p && (*p == '\n' || *p == '\r')) {
                    p++;
                }
                
                if (*p == '-') {
                    while (*p != '\0' && *p == '-') {
                        p++;
                    }
                    //std::cout << "P1: '" << p << "'" << std::flush << std::endl;
                    while (*p != '\0' && (*p == '\n' || *p == '\r')) {                       
                        p++;
                    }
                    state = 1;
                }                
                
                if (strlen(p) > 0 && state > 0 && *p != ' ') {                                       
                    char *p1 = strtok(p, " ");
                    char *parts[11];
                    int i = 0;
                    while (p1 && i<11) {
                        parts[i++] = p1;                        
                        p1 = strtok(NULL," ");
                    }                    
                    auto entry = std::make_shared<RxLogEntry>(parts);                                     
                    log->push_back(entry);                                        
                }               
            }
        }
    }
    std::sort(log->begin(), log->end(), []( const auto &lhs,  const auto &rhs) {
        return lhs->getDate() > rhs->getDate();
    });
    
    return log;
}

std::unique_ptr<std::list<std::string>>MiniC::commandLog() {
    if (!m_port) {
        return NULL;
    }
    cleanInput();
    m_port->purgeBuffer();
    string input = "Message no. 13298   Priority 1   LES id 140   EGC service 31\n\
INFO  413: EGCLOG updated\n\
INFO  463: UTC was updated by GPS\n\
ERROR 253: No printer. Fetch file: REPORT.093 named REPORT.093\n\
INFO   91: Receiving message successful : File EGC.714\n\
Message no. 13745   Priority 1   LES id 140   EGC service 31\n\
INFO  413: EGCLOG updated\n\
INFO  463: UTC was updated by GPS\n\
ERROR 253: No printer. Fetch file: REPORT.094 named REPORT.094\n\
INFO  463: UTC was updated by GPS\n\
ERROR 253: No printer. Fetch file: REPORT.095 named REPORT.095\n\
INFO  463: UTC was updated by GPS\n\
ERROR 253: No printer. Fetch file: REPORT.096 named REPORT.096\n\
INFO   91: Receiving message successful : File EGC.715\n\
Message no. 26430   Priority 1   LES id 112   EGC service 31\n\
INFO  413: EGCLOG updated\n\
INFO  463: UTC was updated by GPS\n";
    cout << "***Commandlog***" << endl;
    std::string::size_type n;
    std::string::size_type nLast;
    std::string lastLine;
    n = 0;
    nLast = n;
    do {
        n = input.find("\n", n + 1);
        if (n && nLast) {
            while (input[n] == '\r' || input[n] == '\n') {
                n++;
            }
            int cnt = n - nLast;
            std::string line = input.substr(nLast, cnt);

            while (line[line.length()-1] == '\r' ||  line[line.length()-1] == '\n') {
                line.erase(line.length()-1,1);
            }
            
            cout << "Line: '" << line << "'" << endl;
            
            lastLine = line;
        } else {
            lastLine.clear();
        }
        nLast = n;
    } while (n != string::npos);
    
    //auto log = std::make_unique<std::list<std::string>>();
    auto log = std::make_unique<std::list<std::string>>();
/*
    char *str = NULL;
    m_port->send("st -w\r\n");
    str = NULL;
    hardware_status.clear();
        
    while (m_port->receive("\r\n", &str) == 0) {
        
    }
*/
    return log;
 }


std::map<std::string, std::string> *MiniC::status() {
    if (!m_port) {
        return NULL;
    }
    cleanInput();
    m_port->purgeBuffer();
    std::map<std::string, std::string> *values = new std::map<std::string, std::string> ();

    char *str = NULL;
    std::cout << "Sending st -i" << std::endl;
    m_port->send("st -i\r\n");
    str = NULL;
    device_info.clear();
    while (m_port->receive("\r\n", &str) == 0) {
        std::cout << "Read:" << str << std::endl;
        if (strlen(str)) {
            if (strstr(str, "tt6194") != NULL) {
                std::cout << "This is a TCU" << std::endl;
                this->tcu = true;
                delete values;
                return NULL;
            } else {                
                stringToMap(str, &device_info);
            }
        }
    }
    std::cout << "Done reading from terminal" << std::endl;
    if (device_info.size() > 0) {
        connected = true;
    }
    // showing contents:
    std::cout << "device_info contains:\n";
    for (auto& kv : device_info) {
        std::cout << kv.first << "=>" << kv.second << '\n';
    }
    return values;
}

std::unique_ptr<vector<char>> MiniC::readfile(string filename) {
    auto buffer = std::make_unique<std::vector<char>>();
    if (m_port) {
        cleanInput();

        char cmd[100];
        snprintf(cmd, sizeof (cmd), "ty -k -h %s\r", filename.c_str());
        m_port->send(cmd);

        char *str = NULL;

        while (m_port->receive("\r", &str) == 0) {
            if (str && strlen(str)) {
                char *p = str;
                if (p) {
                    while (*p && isspace(*p))
                        p++;
                    size_t orig_len = strlen(p);
                    char *src = p;
                    char *end = src + orig_len;
                    char c;                   
                    while (src < end && sscanf(src, "%2X", &c) == 1) {
                        buffer->insert(buffer->end(), c);
                        src += 2;
                    }
                }
            }
        }
    }
    buffer->push_back('\0'); 
    return buffer;
}

bool MiniC::sendBytes(string filename, const char *buffer, size_t cnt) {
    if (!m_port) {
        return false;
    }
    
    m_port->send("\r");
    char *str = NULL;
      while (m_port->receive("\r", &str) == 0) {
    }
    
    char cmd[100];
    snprintf(cmd, sizeof(cmd), "tr %s -b %zu\r", filename.c_str(), cnt);
    m_port->send(cmd);
    m_port->send(buffer);
    // todo: Make sure that tr succeeded (type file -b -k ) ??
    while (m_port->receive("\r", &str) == 0) {            
        if (strlen(str)) {
            std::cout << str << std::endl;
        }
    }

    snprintf(cmd, sizeof(cmd), "tx %s -c 112 -t 6 -e jas\r", filename.c_str());
    int res = m_port->send(cmd);
    
    while (m_port->receive("\r", &str) == 0) {            
        if (strlen(str)) {
            std::cout << str << std::endl;
        }
    }    
    std::cout << "Done sending" << std::endl;
}

bool MiniC::open() {
    if (!m_port) {
        return false;
    }
    char *str = NULL;
    cleanInput();
    m_port->send("\r\n");
    // TODO: Make a proper function to send a command and await a prompt
    // TODO: Get in control over tco/no tcu and reflect in flag
    
    m_port->send("su sysadm\r\n");
        while (m_port->receive(":", &str) == 0) {
        std::cout << "Read:" << str << std::endl;
    }    
    m_port->send("\r\n");
        while (m_port->receive(":", &str) == 0) {
        std::cout << "Read:" << str << std::endl;
    }
    
    m_port->send("minic -f\r\n");

    while (m_port->receive("\r\n", &str) == 0) {
        std::cout << "Read:" << str << std::endl;
    }
    std::cout << "Done reading from terminal" << std::endl;    
    
    std::cout << "Sending st -i" << std::endl;
    m_port->send("st -i\r\n");
    str = NULL;
    
    
    cleanInput();    
    if (((device_info.count("Terminal type") != 0)) &&
            (device_info.at("Terminal type").compare("Inmarsat-C Transceiver") == 0)) {
        return true;
    } else {   
        return false;
    }
}

void MiniC::doCommand(std::string cmd) {
    if (!m_port) {
        return;
    }
    cleanInput();
    char *str = NULL;    
    m_port->send(cmd.c_str());  
    while (m_port->receive("\r\n", &str) == 0) {
        if (strlen(str)) {
            std::cout << "Got from terminal:" << str << std::endl;
        }
    } 
}

bool MiniC::isTcu() const {
    return tcu;
}
/*
 void MiniC::setRegistry(UnitRegistry *registry) { 
     m_registry = registry;
 }
*/
 bool MiniC::isConnected() const {
     return connected;
 }
 
MiniC::~MiniC() {
    delete(m_port);
}

