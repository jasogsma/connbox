import datasource
import json, time, posix_ipc, os

class Model (datasource.DataSource):
    def __init__(self):
        self.in_queue_name = "/frontend-%d" % os.getpid()
        self.mq_out = posix_ipc.MessageQueue("/terminal")
        self.mq_in =  posix_ipc.MessageQueue(self.in_queue_name, posix_ipc.O_CREX)

    def getStatus(self):
        req = {"action":"status", "source": self.in_queue_name}
        self.mq_out.send(json.dumps(req))
        s, _ = self.mq_in.receive()
        s = s.decode()
        print("Received: %s" % s)
        return s

    def getEGCList(self):    
        req = {"action":"EGCList", "source": self.in_queue_name}
        self.mq_out.send(json.dumps(req))
        s, _ = self.mq_in.receive()
        s = s.decode()
        return s

    def getEGC(self, id):
        req = {"action":"GetEGC", "id": id, "source": self.in_queue_name}
        self.mq_out.send(json.dumps(req))
        s, _ = self.mq_in.receive()
        s = s.decode()
        return s

    def sendReport(self, report_data):
        message = "%s %s %s" % (report_data['report.species'],report_data['report.unit'],report_data['report.amount'])
        req = {"action":"sendReport", "data": report_data, "source": self.in_queue_name}
        self.mq_out.send(json.dumps(req))
        s, _ = mq_in.receive()
        s = s.decode()
        print("Received: %s" % s)
        return "OK"

