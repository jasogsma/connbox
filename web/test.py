#!/usr/bin/python3

import json, time, posix_ipc, os

in_queue_name = None
mq_out = None
mq_in =  None

def getEGC(id):
    print("Get EGC")
    req = {"action":"GetEGC", "id": id, "source": in_queue_name}
    mq_out.send(json.dumps(req))
    s, _ = mq_in.receive()
    s = s.decode()
    print("Received: %s" % s)

def status():
    print("Status")
    req = {"action":"status", "source": in_queue_name}
    mq_out.send(json.dumps(req))
    s, _ = mq_in.receive()
    s = s.decode()
    print("Received: %s" % s)
    
def EGCList():
    print("EGC list")
    req = {"action":"EGCList", "source": in_queue_name}
    mq_out.send(json.dumps(req))
    s, _ = mq_in.receive()
    s = s.decode()
    print("Received: %s" % s)
    
in_queue_name = "/frontend-%d" % os.getpid()
mq_out = posix_ipc.MessageQueue("/terminal")
mq_in =  posix_ipc.MessageQueue(in_queue_name, posix_ipc.O_CREX)

status()
getEGC(412)
#EGCList()

    
