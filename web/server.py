#!/usr/bin/env python

from flask import Flask
from flask import request
from flask import jsonify

import json, time, sys, pickle
import simulated
#import livedata
import udsdata

app = Flask(__name__)

model = None
if len(sys.argv) > 1:
    if sys.argv[1] == "simulated":
        print("Using simulated datasource")
        model = simulated.Model()

if model == None:
    print("Using live datasource")
    model = udsdata.Model("/tmp/backend.sock")

@app.route('/api/setup/species',  methods = ['GET'])
def speciesList():
    resp = '{"specieslist":{"1":"Mermaid","2":"Shrimps","3":"Prawns","4":"Cod","5":"Herring","42":"Flounder"}}'
    response = app.response_class(
        response = resp,
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/api/setup/units',  methods = ['GET'])
def unitList():
    resp = '{"units":{"1":"Kilo","2":"Lbs.","3":"Crates"}}'
    response = app.response_class(
        response = resp,
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/api/status',  methods = ['GET'])
def status():
    resp = model.getStatus()
    response = app.response_class(
        response = resp,
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/api/info',  methods = ['GET'])
def info():
    resp = model.getInfo()
    response = app.response_class(
        response = resp,
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/api/egc/<int:egc_id>',  methods = ['GET'])
def GetEGC(egc_id):
    return model.getEGC(egc_id)

@app.route('/api/egclist',  methods = ['GET'])
def EGCList():
    return model.getEGCList()

@app.route('/api/txlog',  methods = ['GET'])
def TxLog():
    return model.getTxLog()

@app.route('/api/rxlog',  methods = ['GET'])
def RxLog():
    return model.getRxLog()

@app.route('/api/report',  methods = ['POST', 'GET'])
def report():
    if request.method == "GET":
        with open("saved-report", "r") as savefile:
            obj = pickle.load(savefile)
        return json.dumps(obj)

    if request.method == "POST" and request.is_json:
        content = request.get_json()
        print("Report received: %s" % content['name'])
        report_data = content['data']
        if content['name'] == "report":
            model.sendReport(report_data)
            os.remove("saved-report")
        if content['name'] == "save-report":
            print(report_data)
            with open("saved-report", "w") as savefile:
                pickle.dump(report_data, savefile)
        return 'Report received'

@app.route('/api/message',  methods = ['POST'])
def message():
    print("Message received")
    if request.is_json:
        content = request.get_json()        
        model.sendMessage(content)
        return 'Report received'

@app.route('/')
def root():
    return app.send_static_file('index3.html')

@app.route('/egc')
def egc():
    return app.send_static_file('egc.html')

@app.route('/api/file/<string:filename>',  methods = ['GET'])
def GetFile(filename):
    return model.getFile(filename)

app.run(host='0.0.0.0',port=8080)

