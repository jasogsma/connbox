import datasource
import json, time, os, socket, sys

class UDSConnection:
    def __init__(self, socketname):
        self.socketname = socketname
        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        try:
            self.socket.connect(socketname)
            self.socket.settimeout(10.0)
        except socket.error, msg:
            print >>sys.stderr, msg
            sys.exit(1)

    def sendMsg(self, message):
        #sent = sock.sendto("\x1e" + message + "\x0a", server_address)
        sent = self.socket.sendall("\x1e" + message + "\x0a")

    def readMsg(self):
        message = None
        data = self.socket.recv(25*1024)
        start = data.find("\x1e")
        end = data.find("\x0a")
        if start > -1 and end > -1:
            message = data[start+1:end]
            # Todo: handle case when end = -1 (no end-eperator read from socket)
        print("Start:%u, end:%u" % (start,end))
        return message

    def close(self):
        self.socket.close()


class Model (datasource.DataSource):
    def __init__(self, socketname):
        self.socketname = socketname
        #self.conn = UDSConnection('/tmp/backend.sock')

    def getStatus(self):
        conn = UDSConnection(self.socketname)
        req = {"action":"hardware-status" }
        conn.sendMsg(json.dumps(req))
        resp = conn.readMsg()
        conn.close()
        return resp


    def getInfo(self):
        conn = UDSConnection(self.socketname)
        req = {"action":"device-info" }
        conn.sendMsg(json.dumps(req))
        resp = conn.readMsg()
        conn.close()
        return resp

    def getEGCList(self):
        conn = UDSConnection(self.socketname)
        req = {"action":"egclist" }
        conn.sendMsg(json.dumps(req))
        resp = conn.readMsg()
        conn.close()
        return resp

    def getTxLog(self):
        conn = UDSConnection(self.socketname)
        req = {"action":"txlog" }
        conn.sendMsg(json.dumps(req))
        resp = conn.readMsg()
        conn.close()
        return resp

    def getRxLog(self):
        conn = UDSConnection(self.socketname)
        req = {"action":"rxlog" }
        conn.sendMsg(json.dumps(req))
        resp = conn.readMsg()
        conn.close()
        return resp

    def getEGC(self, id):
        return None

    def sendReport(self, report_data):
        conn = UDSConnection(self.socketname)
        req = {"action":"sendReport", "data": report_data }
        conn.sendMsg(json.dumps(req))
        resp = conn.readMsg()
        conn.close()
        return resp

    def sendMessage(self, data):
        conn = UDSConnection(self.socketname)
        req = {"action":"sendMessage", "msg": data }
        print(json.dumps(req))
        conn.sendMsg(json.dumps(req))
        resp = conn.readMsg()
        conn.close()
        return resp

    def getFile(self, filename):
        conn = UDSConnection(self.socketname)
        req = {"action":"getfile", "filename": filename }
        conn.sendMsg(json.dumps(req))
        resp = conn.readMsg()
        conn.close()
        return resp
        

    def close(self):
	self.conn.close()

