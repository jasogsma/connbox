/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: stephenaaskov
 *
 * Created on 25. januar 2018, 23:38
 */

#include <cstdlib>
#include "../system/Registry.h"
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    UnitRegistry *registry = new UnitRegistry();
    registry->setup();
    cout << "Dumping registered units:" << endl;
    cout << registry->numTerminals() << " Terminals registered" << endl;
    for (unsigned int i=0;i<registry->numTerminals();i++) {
        struct terminal_info *terminal = registry->getTerminalEntry(i);
        cout << "\tType:" << terminal->terminal_type << endl;
        cout << "\tSerial number:" << terminal->serial_number << endl;
        cout << "\tMobile number:" << terminal->mobile_number << endl;
        cout << "\tISN number:" << terminal->isn_number << endl;
        cout << "\tMobile type:" << terminal->mobile_type << endl;
        cout << "\tUnit PID:" << terminal->pid<< endl;
        cout << "\tStatusJSON:" << terminal->statusJSON << endl;
    }
    return 0;
}

