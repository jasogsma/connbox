/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: stephenaaskov
 *
 * Created on 31. januar 2018, 19:21
 */

#include <cstdlib>
#include <iostream>
#include "fcgio.h"
#include "json.hpp"
#include "Registry.h"

using namespace std;
using json = nlohmann::json;

/*
 * 
 */
int main(void) {
    // Backup the stdio streambufs
    streambuf * cin_streambuf  = cin.rdbuf();
    streambuf * cout_streambuf = cout.rdbuf();
    streambuf * cerr_streambuf = cerr.rdbuf();

    FCGX_Request request;

    FCGX_Init();
    FCGX_InitRequest(&request, 0, 0);
    
    UnitRegistry *registry = new UnitRegistry();
    registry->setup();
    
/*
    json j;
    j["pi"] = 3.141;
    j["happy"] = true;
    j["name"] = "Niels";
    j["nothing"] = nullptr;
    j["answer"]["everything"] = 42;
    j["list"] = {1, 0, 2};
    j["object"] = {
        {"currency", "USD"},
        {"value", 42.99}
    };
    std::string s = j.dump(); 
*/
    while (FCGX_Accept_r(&request) == 0) {
        
        const char * uri = FCGX_GetParam("REQUEST_URI", request.envp);
        std::cout << "REQUEST_URI:" << uri << std::endl;
        
        fcgi_streambuf cin_fcgi_streambuf(request.in);
        fcgi_streambuf cout_fcgi_streambuf(request.out);
        fcgi_streambuf cerr_fcgi_streambuf(request.err);

        cin.rdbuf(&cin_fcgi_streambuf);
        cout.rdbuf(&cout_fcgi_streambuf);
        cerr.rdbuf(&cerr_fcgi_streambuf);

        cout << "Content-type: text/html\r\n"
             << "\r\n"
             << "<html>\n"
             << "  <head>\n"
             << "    <title>Hello, World!</title>\n"
             << "  </head>\n"
             << "  <body>\n"
             << "    <h1>Hello, World!</h1>\n";
        cout << "<pre>";
    for (unsigned int i=0;i<registry->numTerminals();i++) {
        struct terminal_info *terminal = registry->getTerminalEntry(i);
        cout << "\tType:" << terminal->terminal_type << endl;
        cout << "\tSerial number:" << terminal->serial_number << endl;
        cout << "\tMobile number:" << terminal->mobile_number << endl;
        cout << "\tISN number:" << terminal->isn_number << endl;
        cout << "\tMobile type:" << terminal->mobile_type << endl;
        cout << "\tUnit PID:" << terminal->pid<< endl;
        cout << "\tStatusJSON:" << terminal->statusJSON << endl;
    }        
        cout << "</pre>"
             << "  </body>\n"
             << "</html>\n";

        // Note: the fcgi_streambuf destructor will auto flush
    }

    // restore stdio streambufs
    cin.rdbuf(cin_streambuf);
    cout.rdbuf(cout_streambuf);
    cerr.rdbuf(cerr_streambuf);

    return 0;
}
