/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BackendServer.h
 * Author: stephenaaskov
 *
 * Created on 14. maj 2018, 23:46
 */

#include "../miniC/MiniC.h"
#include "UDSServer.h"
#include <string>
#include "../miniC/MiniC.h"
#include "json.hpp"

using json = nlohmann::json;

#ifndef BACKENDSERVER_H
#define BACKENDSERVER_H

class BackendServer : public UDSServer {
public:
    BackendServer(string socketname, MiniC *terminal);
    BackendServer(const BackendServer& orig);
    virtual void handleMessage(int socket, json msg);
    void handleTxLog(json& response);
    void handleRxLog(json& response);
    void cutHeaderField(string content, string key, json& response);
    virtual ~BackendServer();
private:
  MiniC *m_terminal = NULL;
};

#endif /* BACKENDSERVER_H */

