/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BackendServer.cpp
 * Author: stephenaaskov
 * 
 * Created on 14. maj 2018, 23:46
 */

#include "BackendServer.h"
#include "../miniC/MiniC.h"
#include "EGCListEntry.h"
#include <unistd.h>
#include <iostream>
#include <locale>         // std::locale, std::tolower
#include <ctime>
#include "json.hpp"

using json = nlohmann::json;



BackendServer::BackendServer(string socketname, MiniC *terminal) : UDSServer(socketname) {
    m_terminal = terminal;
#if 0
      // showing contents:
    std::cout << "device_info:\n";
    for (auto& kv : m_terminal->device_info) {
        std::cout << kv.first << "=>" << kv.second << '\n';
    }      
    std::cout << std::flush;
#endif    
}

/*
BackendServer::BackendServer(const BackendServer& orig) {
}
 */


void BackendServer::handleTxLog(json& response)
{
        auto log = m_terminal->TXLog();
        std::cout << "TXLog" << std::endl;
        
        response["response"] = "txlog";
        response["terminal-connected"] = m_terminal->isConnected();
 
        auto jsonObjects = json::array();           
        json hardwareStatus(m_terminal->hardware_status);
        
        for (auto const & kv : *log) {
            json j;
            j["status"] = kv->GetStatus(); 
            j["fileref"] = kv->GetFileRef();
            j["size"] = kv->GetSize();
            j["date"] = kv->GetDate();
            j["time"] = kv->GetTime();
            j["destination"] = kv->GetDestination();
            j["LES"] = kv->GetLes();
            
            // 14-05-23",Time:"14:55"
            string dateTimeStr = string(kv->GetDate() + " " + kv->GetTime());
            std::tm t = {};
            std::istringstream ss(dateTimeStr);
            // https://en.cppreference.com/w/cpp/io/manip/get_time
            time_t ret = 0;
            ss >> std::get_time(&t, "%y-%m-%d %H:%M");
            if (!ss.fail()) {
                t.tm_year += 100;
                char *tz = getenv("TZ");
                setenv("TZ", "", 1);
                //tzset();
                ret = std::mktime(&t);
                if(tz)
                    setenv("TZ", tz, 1);
                j["timestamp"] = ret;
            }
            
            jsonObjects.push_back(j);                
        }
        response["txlog"] = jsonObjects;
}


void BackendServer::handleRxLog(json& response)
{
        auto log = m_terminal->RXLog();
        std::cout << "RXLog" << std::endl;
        
        response["response"] = "rxlog";
        response["terminal-connected"] = m_terminal->isConnected();
 
        auto jsonObjects = json::array();           
        json hardwareStatus(m_terminal->hardware_status);
        
        for (auto const & kv : *log) {
            json j;
            j["status"] = kv->getStatus(); 
            j["file"] = kv->getFile();
            j["size"] = kv->getSize();
            j["messageNo"] = kv->getMessageNo();
            j["date"] = kv->getDate();
            j["time"] = kv->getTime();
            j["LES"] = kv->getLES();            

            // 14-05-23",Time:"14:55"
            string dateTimeStr = string(kv->getDate() + " " + kv->getTime());
            std::tm t = {};
            std::istringstream ss(dateTimeStr);
            // https://en.cppreference.com/w/cpp/io/manip/get_time
            time_t ret = 0;
            ss >> std::get_time(&t, "%y-%m-%d %H:%M");
            if (!ss.fail()) {
                t.tm_year += 100;
                char *tz = getenv("TZ");
                setenv("TZ", "", 1);
                tzset();
                ret = std::mktime(&t);
                if(tz)
                    setenv("TZ", tz, 1);
                j["timestamp"] = ret;
            }
            
            jsonObjects.push_back(j); 
        }
        response["rxlog"] = jsonObjects;
}

void BackendServer::cutHeaderField(string content, string key, json& response)
{
    std::size_t found = content.find(key + ": ");
    if (found != string::npos) {
        std::size_t nlpos = content.find("\r\n", found);

        if (nlpos != string::npos) {
            std::locale loc;
            for (std::string::size_type i=0; i<key.length(); ++i)
                key[i] = std::tolower(key[i],loc);            
            response[key] = content.substr(found + key.length() + 2, nlpos - found - (key.length() + 2));
        }
    }
}

/**
 * Called for message reveived from web server
 * @param socket Connection
 * @param msg Message received
 */
void BackendServer::handleMessage(int socket, json msg) { 
    json response = "{ \"result\": \"success\" }"_json;

    if (msg.find("action") != msg.end()) {
        if (string("get_info").compare(msg["action"]) == 0) {
            std::cout << "get_info request" << std::endl;
            if (m_terminal != NULL) {
                json device_info(m_terminal->device_info);
                response["data"] = device_info;
            }
        }
        
        if (string("sendReport").compare(msg["action"]) == 0) {
            std::cout << "Report requested" << std::endl;
            if (m_terminal != NULL) {
                stringstream reportText;
                reportText << "Catch report:";
                json report = msg["data"];
                json catchList = report["catchList"];
                reportText << std::endl;
                if (catchList.is_array()) {

                    for (int i = 0; i < catchList.size(); i++) {
                        std::cout << i << ":" << catchList[i] << std::endl;
                        reportText << catchList[i]["amount"].get<std::string>() << " " << catchList[i]["unit"].get<std::string>() << " of " << catchList[i]["species"].get<std::string>() << ", Covering " << catchList[i]["days"].get<std::string>() << " days" << std::endl;
                    }
                }
                std::cout << reportText.str() << std::endl;
                m_terminal->sendBytes("report.txt",
                        reportText.str().c_str(),
                        reportText.str().length());
                std::cout << "Sent report" << std::endl << std::flush;
                response["response"] = "device-info";
                //response["terminal-connected"] = m_terminal->isConnected();            
            }
        }


        if (string("sendMessage").compare(msg["action"]) == 0) {
            std::cout << "SendMessage requested" << std::endl;
            if (m_terminal != NULL) {
                stringstream reportText;
                json data = msg["msg"];
                std::cout << data["subject"].get<std::string>() << std::endl;
                
                reportText << "__" << data["subject"].get<std::string>() << "__" << std::endl;
                reportText << data["message"].get<std::string>();
                reportText << std::endl;
                std::cout << reportText.str() << std::endl;
                m_terminal->sendBytes("message.txt",
                        reportText.str().c_str(),
                        reportText.str().length());
                std::cout << "Sent message" << std::endl << std::flush;
 
                response["response"] = "device-info";
              //  response["terminal-connected"] = m_terminal->isConnected();            
            }
        }
        
        
        if (string("device-info").compare(msg["action"]) == 0) {   
            std::cout << "device-info requested" << std::endl;
            m_terminal->status();
            response["response"] = "deviceinfo";
            //response["terminal-connected"] = m_terminal->isConnected();
            
            for (auto& kv : m_terminal->device_info) {
                std::cout << kv.first << "=>" << kv.second << '\n';
            }
            
            json deviceInfo(m_terminal->device_info);
            response["deviceinfo"] = deviceInfo; 
        }
        
        if (string("hardware-status").compare(msg["action"]) == 0) {   
            m_terminal->hardwareStatus();
            std::cout << "hardware-status requested" << std::endl;
            response["response"] = "hardwarestatus";
            //response["terminal-connected"] = m_terminal->isConnected();

            for (auto& kv : m_terminal->hardware_status) {
                std::cout << kv.first << "=>" << kv.second << '\n';
            }
            
            json hardwareStatus(m_terminal->hardware_status);
            response["hardwarestatus"] = hardwareStatus;             
        }


        if (string("egclist").compare(msg["action"]) == 0) {
            auto EGCList = m_terminal->listEGC();
            std::cout << "EGC list" << std::endl;
            for (auto const & kv : *EGCList) {
                std::cout << "EGC " << kv.first << " has time " << kv.second->GetDate() << std::endl;
            }
            response["response"] = "egclist";
            //response["terminal-connected"] = m_terminal->isConnected();
 
            auto jsonObjects = json::array();
            
            json hardwareStatus(m_terminal->hardware_status);

            std::cout << "EGC list" << std::endl;
            for (auto const & kv : *EGCList) {
                json j;
                j["number"] = kv.second->GetNumber(); 
                j["data"] = kv.second->GetDate();
                j["size"] = kv.second->GetSize();
                j["time"] = kv.second->GetTime();
                // Hmm..... the elegant way does not work. Need to put stuff in namespaces...
                std::cout << "EGC " << kv.first << " JSON: " << j.dump() << std::endl;
                jsonObjects.push_back(j);                
            }
            response["egclist"] = jsonObjects;
        }

        if (string("txlog").compare(msg["action"]) == 0) {        
            handleTxLog(response);
        }
        
        if (string("rxlog").compare(msg["action"]) == 0) {        
            handleRxLog(response);
        }
        
        if (string("getfile").compare(msg["action"]) == 0) {
            response["response"] = "getfile";
            auto it = msg.find("filename");
            if (it != msg.end()) {
                string filename = msg["filename"];
                auto data = m_terminal->readfile(msg["filename"]);
                char *p = data->data();
                string fileContent = string(data->data()); 
                // Extract "From" and "Subject" fields into variables in the JSON object
                cutHeaderField(fileContent, "From", response);
                cutHeaderField(fileContent, "Subject", response);
                std::size_t found = fileContent.find("\r\n\r\n");
                if (found != string::npos) {
                    fileContent.erase(0, found + 4);
                }
                
                response["file"] = fileContent;
                response["result"] = "OK";
            } else {
                std::cout << "getfile: No filename in request" << std::endl;
                response["result"] = "ERROR";
            }
        }        
        
    }
    response["terminal-connected"] = m_terminal->isConnected(); 
    sendMessage(socket, response);
}


BackendServer::~BackendServer() {
}

