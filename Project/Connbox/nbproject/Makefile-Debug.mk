#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/6114b52c/MiniC.o \
	${OBJECTDIR}/_ext/f88f24c/SerialPort.o \
	${OBJECTDIR}/_ext/cca2efcf/EGCListEntry.o \
	${OBJECTDIR}/_ext/cca2efcf/QueueServer.o \
	${OBJECTDIR}/_ext/cca2efcf/RxLogEntry.o \
	${OBJECTDIR}/_ext/cca2efcf/TxLogEntry.o \
	${OBJECTDIR}/_ext/cca2efcf/UDSServer.o \
	${OBJECTDIR}/BackendServer.o \
	${OBJECTDIR}/CommandLogEntry.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Wl,-rpath,'.'

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/connbox

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/connbox: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/connbox ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/6114b52c/MiniC.o: ../../miniC/MiniC.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6114b52c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6114b52c/MiniC.o ../../miniC/MiniC.cpp

${OBJECTDIR}/_ext/f88f24c/SerialPort.o: ../../serial_port/SerialPort.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/f88f24c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f88f24c/SerialPort.o ../../serial_port/SerialPort.cpp

${OBJECTDIR}/_ext/cca2efcf/EGCListEntry.o: ../../system/EGCListEntry.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/cca2efcf
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cca2efcf/EGCListEntry.o ../../system/EGCListEntry.cpp

${OBJECTDIR}/_ext/cca2efcf/QueueServer.o: ../../system/QueueServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/cca2efcf
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cca2efcf/QueueServer.o ../../system/QueueServer.cpp

${OBJECTDIR}/_ext/cca2efcf/RxLogEntry.o: ../../system/RxLogEntry.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/cca2efcf
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cca2efcf/RxLogEntry.o ../../system/RxLogEntry.cpp

${OBJECTDIR}/_ext/cca2efcf/TxLogEntry.o: ../../system/TxLogEntry.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/cca2efcf
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cca2efcf/TxLogEntry.o ../../system/TxLogEntry.cpp

${OBJECTDIR}/_ext/cca2efcf/UDSServer.o: ../../system/UDSServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/cca2efcf
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cca2efcf/UDSServer.o ../../system/UDSServer.cpp

${OBJECTDIR}/BackendServer.o: BackendServer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BackendServer.o BackendServer.cpp

${OBJECTDIR}/CommandLogEntry.o: CommandLogEntry.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CommandLogEntry.o CommandLogEntry.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../serial_port -I../../system -I../../3rdparty/json/src -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
