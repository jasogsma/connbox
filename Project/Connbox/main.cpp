/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: stephenaaskov
 *
 * Created on 21. januar 2018, 16:47
 */

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <new>
#include <memory>
#include <signal.h>
#include <vector>
#include "../miniC/MiniC.h"
#include "BackendServer.h"
#include <unistd.h>

using namespace std;

void usage() {
    std::cout << "connbox backend. Usage:" << std::endl;
    std::cout << "  -d serial device" << std::endl;
}

int main(int argc, char** argv) {
    int ch;

    char *devicename = NULL;
    while ((ch = getopt(argc, argv, "?d:")) != -1) {
        switch (ch) {
            case 'd':
                devicename = optarg;
                break;
            case '?':
            default:
                usage();
                exit(1);
        }
    }
    argc -= optind;
    argv += optind;

    if (!devicename) {
        usage();
        exit(1);
    }
    
    signal(SIGPIPE, SIG_IGN);
    std::cout << "Opening device " << devicename << " for serial comms." << std::endl;
    MiniC *mini_c = new MiniC(devicename);
    bool res = mini_c->open();
    
    if (mini_c->isTcu()) {
      std::cout << "TCU: yes" << std::endl;
    }
    else  {
        std::cout << "TCU: no" << std::endl;
    }
    if (mini_c->isTcu()) {
        std::cout << "Connected to TCU" << std::endl;
        mini_c->doCommand("minic\r\n");
        sleep(10);
        mini_c->status();
    }
    //mini_c->commandLog();
    mini_c->hardwareStatus();
    //mini_c->RXLog();
    
#if 0
    std::cout << "Reading file" << std::endl;
    auto data = mini_c->readfile("EGC.507");
    char *p = data->data();
    std::cout << "Read:" << p << std::endl;    
#endif
    
#if 0
    std::cout << "Reading file" << std::endl;
    std::vector<char> data = mini_c->readfile("EGC.507");
    char *p = data.data();
    p = strtok(p,"\r\n");
    if (p) {
        p = strtok(NULL,"\r\n");
    //std::cout << "Read:" << p << std::endl;
    char *src = p;
    char *dst = src;
    char *end = src + data.size();
    char c;
    while (dst < end && sscanf(src, "%2X", &c) == 1)
    {
        *dst++ = c;
        src += 2;
    }
    *dst = 0;
      std::cout << ":" << p << std::endl;
    }
#endif
   

    std::cout << "Starting backend server" << std::endl;
    BackendServer server("/tmp/backend.sock", mini_c);
    server.start();
    server.messageloop();
    
#if 0
    QueueServer server;
    server.setTerminal(mini_c);
    server.open();
    server.run();        
#endif
    
    do {
        std::cout << '\n' << "Press a key to continue...";
    } while (std::cin.get() != '\n');
    
    return 0;
}
