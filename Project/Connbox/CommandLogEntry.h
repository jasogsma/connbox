/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CommandLogEntry.h
 * Author: stephenaaskov
 *
 * Created on 30. maj 2018, 22:57
 */

#ifndef COMMANDLOGENTRY_H
#define COMMANDLOGENTRY_H

#include <string>

using namespace std;

/*
 Message no. 13298   Priority 1   LES id 140   EGC service 31
INFO  413: EGCLOG updated
INFO  463: UTC was updated by GPS
ERROR 253: No printer. Fetch file: REPORT.093 named REPORT.093
INFO   91: Receiving message successful : File EGC.714
Message no. 13745   Priority 1   LES id 140   EGC service 31
INFO  413: EGCLOG updated
INFO  463: UTC was updated by GPS
ERROR 253: No printer. Fetch file: REPORT.094 named REPORT.094
INFO  463: UTC was updated by GPS
ERROR 253: No printer. Fetch file: REPORT.095 named REPORT.095
INFO  463: UTC was updated by GPS
ERROR 253: No printer. Fetch file: REPORT.096 named REPORT.096
INFO   91: Receiving message successful : File EGC.715
Message no. 26430   Priority 1   LES id 112   EGC service 31
INFO  413: EGCLOG updated
INFO  463: UTC was updated by GPS
 */
//const unsigned int ENTRY_TYPE_INFO = 1;
//const unsigned int ENTRY_TYPE_ERROR = 2;


class CommandLogEntry {
public:
    enum {ENTRY_TYPE_INFO=1, ENTRY_TYPE_ERROR };
    CommandLogEntry(string text);
//    CommandLogEntry(const CommandLogEntry& orig);
    virtual ~CommandLogEntry();
private:
    unsigned int m_type;
    unsigned int m_code;
    // ..some map to hold message specific data items
    string m_message;
    string m_additional;
};

#endif /* COMMANDLOGENTRY_H */

