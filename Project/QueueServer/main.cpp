/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: stephenaaskov
 *
 * Created on 11. februar 2018, 21:54
 */

#include <cstdlib>
#include "QueueServer.h"
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    QueueServer server;
    server.open();
    server.run();
    return 0;
}

