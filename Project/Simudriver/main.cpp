/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: stephenaaskov
 *
 * Created on 1. februar 2018, 21:38
 */

#include <cstdlib>
#include <iostream>
#include "../../system/Registry.h"
#include "Simu.h"

using namespace std;

/*
 * 
 */
UnitRegistry *registry = NULL;

int main(int argc, char** argv) {
    registry = new UnitRegistry();
    registry->setup();
    Simu simulatedTerminal(registry);
    
    do {
        std::cout << '\n' << "Press a key to continue...";
    } while (std::cin.get() != '\n');

    return 0;
}

