/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Simu.cpp
 * Author: stephenaaskov
 * 
 * Created on 1. februar 2018, 22:43
 */

#include "Simu.h"
#include <cstdio>
#include <cstring>
#include <iostream>
#include "json.hpp"
#include "../../system/Registry.h"

using json = nlohmann::json;


Simu::Simu(UnitRegistry *registry) {
    terminal_info info;
    info.type = TERMINAL_TYPE_SIMULATED;
    snprintf(info.mobile_number, sizeof (info.mobile_number),
            "12345678");
    snprintf(info.serial_number, sizeof (info.serial_number),
            "11223344");
    snprintf(info.isn_number, sizeof (info.isn_number),
            "424242");
    snprintf(info.mobile_type, sizeof (info.mobile_number),
            "SatCom");
    snprintf(info.terminal_type, sizeof (info.terminal_type),
            "Simulated SatCom");
    std::cout << "SimuTerminal created" << std::endl;
    info.pid = getpid();
    
    m_registry = registry;
    if(m_registry != NULL) {
        m_terminalRegistration = m_registry->addTerminal(&info);
    }
    doStatGather();
}

void Simu::doStatGather() {

    json j;

    j["status"] = "logged in";
    j["SNR"] = 43.1;
    j["region"] = "EMEA";
    j["latitude"] = 9.035;
    j["longitude"] = 55.032;    
  
    std::string s = j.dump(); 
    if (m_terminalRegistration != NULL) {
        snprintf(m_terminalRegistration->statusJSON, sizeof(m_terminalRegistration->statusJSON), s.c_str());
    }
}

Simu::Simu(const Simu& orig) {
}

Simu::~Simu() {
}

