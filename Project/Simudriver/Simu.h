/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Simu.h
 * Author: stephenaaskov
 *
 * Created on 1. februar 2018, 22:43
 */

#ifndef SIMU_H
#define SIMU_H

#include "../../system/Registry.h"

class Simu {
public:
    Simu(UnitRegistry *registry);
    Simu(const Simu& orig);
    void doStatGather();
    virtual ~Simu();
private:
    UnitRegistry *m_registry;
    struct terminal_info *m_terminalRegistration;
};

#endif /* SIMU_H */

