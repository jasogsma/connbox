/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Registry.h
 * Author: stephenaaskov
 *
 * Created on 24. januar 2018, 23:08
 */

#ifndef REGISTRY_H
#define REGISTRY_H

#include <cstdint>
#include <unistd.h>

#define NUM_TERMINALS 5
#define STATUS_JSON_LENGTH 200


enum terminal_type {
    TERMINAL_TYPE_NONE,
    TERMINAL_TYPE_MINI_C,
    TERMINAL_TYPE_SIMULATED
};

struct terminal_info {
    enum terminal_type type;
    char isn_number[15];
    char serial_number[15];
    char mobile_number[15];
    char mobile_type[50];
    char terminal_type[50];
    char statusJSON[STATUS_JSON_LENGTH];
    pid_t pid;
};

struct registry_area {
    uint32_t magic;
    unsigned int num_terminals;
    struct terminal_info terminals[NUM_TERMINALS];
};

class UnitRegistry {
public:
    UnitRegistry();
    //UnitRegistry(const Registry& orig) = delete;
    virtual ~UnitRegistry();
    bool setup();
    struct terminal_info *addTerminal(struct terminal_info *info);
    unsigned int numTerminals();
    struct terminal_info *getTerminalEntry(unsigned int idx);
private:
    const uint32_t REGISTRY_MAGIC_VALUE = 0xabe43;
    int shared_fd = -1;
    struct registry_area *m_sharedArea;
    int sharedSize = 0;
};

#endif /* REGISTRY_H */

