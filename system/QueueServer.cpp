/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   QueueServer.cpp
 * Author: stephenaaskov
 * 
 * Created on 11. februar 2018, 21:59
 */
#include <iostream>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
//#include <mqueue.h>
#include <cstring>
#include <sstream>
#include <string>
#include "QueueServer.h"
#include "json.hpp"
#include "EGCListEntry.h"
#include <unistd.h>

using json = nlohmann::json;

QueueServer::QueueServer() {
}

QueueServer::QueueServer(const QueueServer& orig) {
}

QueueServer::~QueueServer() {
#if 0
    if (m_mqd != -1){
        mq_close(m_mqd);
    }
    mq_unlink("/terminal");
#endif
}

bool QueueServer::open() {
#if 0
 struct mq_attr  attr;
 
 attr.mq_maxmsg = 10;
 attr.mq_msgsize = 512;
 
 m_mqd = mq_open("/terminal", O_RDWR|O_CREAT, S_IRWXU, &attr);
 if(m_mqd == -1) {
      std::cout << "mq_open:" << strerror(errno) << std::endl;
 }
#endif
}

void QueueServer::run() {
    /*
    char buffer[512 + 1];
    std::cout << "Starting queue server" << std::endl;
    while (true) {
        if (m_mqd == -1) {
            std::cout << "Queue not open" << std::endl;
            return;
        }

        ssize_t count = mq_receive(m_mqd, &buffer[0],
                sizeof (buffer), NULL);
        if (count == -1) {
            std::cout << strerror(errno) << std::endl;
            return;
        }

        buffer[count] = 0;
        std::cout << "Received(" << count << "):" << buffer << std::endl;
        json msg;
        try {
          msg = json::parse(buffer);
        }
        catch(nlohmann::detail::parse_error e) {
            std::cout << "JSON Parse error" << std::endl;
            
        }
        if(msg.is_null()) {
            std::cout << "JSON object is null" << std::endl;
        } else {
            handleMessage(msg);
        }
    }
     */
}

void QueueServer::setTerminal(MiniC *terminal)
{
//    m_terminal = terminal;
}

bool QueueServer::sendText(string recepient, const char *text) {
    /*
    struct mq_attr attr;

    attr.mq_maxmsg = 10;
    attr.mq_msgsize = 512;

    if (!text) {
        return false;
    }
   std::cout << "sendText:" << text << endl;
    mqd_t mqd = mq_open(recepient.c_str(), O_RDWR, S_IRWXU, &attr);
    if (mqd == -1) {
        std::cout << strerror(errno) << std::endl;
    }
    else {
        mq_send(mqd, text, strlen(text), 0);
    }
    std::cout << "text message sent to : " << recepient.c_str() << std::endl;
     */
}



bool QueueServer::sendJson(string recepient, json msg) {
    /*
    struct mq_attr attr;

    attr.mq_maxmsg = 10;
    attr.mq_msgsize = 512;

    mqd_t mqd = mq_open(recepient.c_str(), O_RDWR, S_IRWXU, &attr);
    if (mqd == -1) {
        std::cout << strerror(errno) << std::endl;
    }
    else {
        std::string serialized_string = msg.dump();
        mq_send(mqd,serialized_string.c_str(),serialized_string.length(), 0);
    }
     */
}

void QueueServer::handleMessage(json msg) {
#if 0
    usleep(10000);
    
    if (!m_terminal) {
        std::cout << __func__ << ":No terminal object!" << std::endl;
        return;
    }

    if (std::string("GetEGC").compare(msg["action"]) == 0) {
        cout << "id:" << msg["id"] << endl;
        if (msg["id"] != nullptr) {
            char filename[13];
            snprintf(filename,sizeof(filename),"EGC.%u",(unsigned int)msg["id"]);
            auto data = m_terminal->readfile(filename);
            string str(data->data());

            sendText(msg["source"], str.c_str());
        } else {
            sendText(msg["source"], "ERROR!");
        }
    }
    
    if (std::string("EGCList").compare(msg["action"]) == 0) {
        json json_list;
        auto EGCList = m_terminal->listEGC();        
        for (auto const & kv : *EGCList) {
            json entry;
            entry["time"] = kv.second->GetTime();
            entry["date"] = kv.second->GetDate();
            json_list[std::to_string(kv.first)] = entry;
            std::cout << "EGC " << kv.first << " has time " << kv.second->GetDate() << std::endl;            
        }       
        json response;
        response["response"] = "EGCList";
        response["data"] = json_list;
        sendJson(msg["source"], response);
    }
    
    if (std::string("sendReport").compare(msg["action"]) == 0) {
        std::cout << "Report requested" << std::endl;
        if (m_terminal != NULL) {
            stringstream reportText;
            reportText << "Catch report:";
            json report = msg["data"];
            json catchList = report["catchList"];
            reportText << std::endl;
            if (catchList.is_array()) {                

                for (int i = 0; i < catchList.size(); i++) {
                    std::cout << i << ":" << catchList[i] << std::endl;
                    reportText << catchList[i]["amount"].get<std::string>() << " " << catchList[i]["unit"].get<std::string>() << " of " <<  catchList[i]["species"].get<std::string>() << std::endl;
                }
            }
            std::cout << reportText.str() << std::endl;
            m_terminal->sendBytes("report.txt",
                    reportText.str().c_str(),
                    reportText.str().length());
        }
    }

    if (std::string("status").compare(msg["action"]) == 0) {
        std::cout << "Status data requested" << std::endl;

        json response;
        response["response"] = "status";
        response["terminal-connected"] = m_terminal->isConnected();
        // showing contents:
        //std::cout << "device_info contains:\n";
        for (auto& kv : m_terminal->device_info) {
            std::cout << kv.first << "=>" << kv.second << '\n';
        }
        json deviceInfo(m_terminal->device_info);
        response["device-info"] = deviceInfo;
        sendJson(msg["source"], response);

    }
#endif
}