/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RxLogEntry.cpp
 * Author: stephenaaskov
 * 
 */

#include "RxLogEntry.h"
#include <string>
#include <cstring>
#include <iostream>

using namespace std;

/*
LES Sv P L       Time      Bytes Destination    MTCA Status  File/Ref
----------------------------------------------------------------------
112  0 1 0 13-10-24  10:59  1114 28             0610 Deliver 374353 
 */ 
/*
TxLogEntry::RxLogEntry(char *line) {
    unsigned int fieldNum = 0;  
    char *p = strtok(line, " ");
    while (p) {
        switch(fieldNum) {
            case 9:
                m_status = string(p); // use smart pointer instead
                break;
            case 10:
                m_fileRef = string(p);
        }
        p = strtok(NULL," ");
        fieldNum++;
    }
}

*/

RxLogEntry::RxLogEntry(char *parts[]) {
    m_les = parts[0];
    m_date = parts[4];
    m_time = parts[5];
    m_size = parts[6];
    m_messageNo = parts[7];
    m_status = parts[9];
    m_file = parts[10];
}

/*
TxLogEntry::RxLogEntry(const TxLogEntry& orig) {
}
*/
RxLogEntry::~RxLogEntry() {
}

string RxLogEntry::getLES() {
    return m_les;
}

string RxLogEntry::getDate() {
    return m_date;
}

string RxLogEntry::getTime() {
    return m_time;
}

string RxLogEntry::getMessageNo() {
    return m_messageNo;
}

string RxLogEntry::getStatus() {
    return m_status;
}

string RxLogEntry::getSize() {
    return m_size;
}

string RxLogEntry::getFile() {
    return m_file;
}
