/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UDSServer.cpp
 * Author: stephenaaskov
 * 
 * Created on 22. april 2018, 22:28
 */

#include "UDSServer.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include "json.hpp"

//#define SOCKET_NAME "/tmp/backend.sock"
#define BUFFER_SIZE 1024

using json = nlohmann::json;
using namespace std;

UDSServer::UDSServer(string socketname) {
    m_socketname = socketname;
}

int UDSServer::start() {
    int ret = 0;
    
    unlink(m_socketname.c_str());
    m_serversocket = socket(AF_UNIX, SOCK_STREAM, 0);
    if (m_serversocket == -1) {
        perror("socket");
        return 1;
    }

    struct sockaddr_un name;
    memset(&name, 0, sizeof (struct sockaddr_un));

    name.sun_family = AF_UNIX;
    strncpy(name.sun_path, m_socketname.c_str(), sizeof (name.sun_path) - 1);

    ret = ::bind(m_serversocket, (const struct sockaddr *) &name,
            sizeof (struct sockaddr_un));
    if (ret == -1) {
        perror("bind");
        return 2;
    }
    
    ret = listen(m_serversocket, 20);
    if (ret == -1) {
        perror("listen");
        return 3;
    }
    
    return 0;
}


void UDSServer::sendMessage(int socket, json msg) {
    if (socket < 0) {
        return;
    }
    std::cout << __func__ << ":" << msg.dump() << std::endl;
    char response[50*1024];
    ssize_t to_send = snprintf(&response[0], sizeof(response),"%c%s%c", 0x1e, msg.dump().c_str(), 0x0a);        
    cout << __func__ << ": to_send:" << to_send << std::endl;
    if(to_send >= sizeof(response)) {
        cout << __func__ << ":Buffer overflow!" << std::endl;
    }
    ssize_t sent_bytes = 0;
    while (sent_bytes < to_send) {        
        ssize_t cnt = send(socket, &response[sent_bytes], to_send - sent_bytes, 0);
        cout << __func__ << ": Sent: " << cnt << std::endl;
        if (cnt < 0) {            
            return;
        }
        sent_bytes += cnt;
    }
}

void UDSServer::handleMessage(int socket, json msg) {
    sendMessage(socket, msg);
}

int UDSServer::messageloop() {
    if (m_serversocket < 0) {
        return 1;
    }
    
    for (;;) {
        struct sockaddr_un claddr;
        socklen_t addrlen = sizeof (sockaddr_un);
        int client_socket = accept(m_serversocket, (struct sockaddr *)&claddr, &addrlen);
        if (client_socket == -1) {
            perror("accept");
            continue;
        }
        
        //int set = 1;
        //setsockopt(client_socket, SOL_SOCKET, SO_NOSIGPIPE, (void*)&set, sizeof(int));
        
        std::cout << "New connection" << std::endl;
        char buffer[BUFFER_SIZE];
        size_t read_count = 0;
        ssize_t ret = 0;
        while (ret > -1) {
            socklen_t len = sizeof (struct sockaddr_un);
            ret = read(client_socket, buffer + read_count, BUFFER_SIZE - read_count);
            if (ret == -1) {
                perror("read");
                close(client_socket);
                client_socket = -1;
                break;
            }
            if (ret == 0) {
                std::cout << "Done with client" << std::endl;
                close(client_socket);
                client_socket = -1;
            }
            
            buffer[read_count + ret] = 0;
            read_count += ret;
            char *start = strchr(buffer, 0x1e);
            char *end = strchr(buffer, 0x0a);
            if (start && end && (end > start)) {
                start++;
                *end = 0;
                // Parsse json and handle
                json msg;
                try {
                    msg = json::parse(start);
                } catch (nlohmann::detail::parse_error e) {
                    std::cout << "JSON Parse error" << std::endl;
                    json resp = "{ \"error\": \"Parsing request\" }"_json;
                    sendMessage(client_socket, resp);
                }
                if (msg.is_null()) {
                    std::cout << "JSON object is null" << std::endl;
                } else {
                    handleMessage(client_socket, msg);
                }

                
               // handleMessage(client_socket, msg);
                
                if (read_count > end + 1 - buffer) {
                    int cnt = read_count - (end + 1 - buffer);
                    memcpy(buffer, end + 1, cnt);
                    buffer[cnt] = 0;
                    read_count = cnt;
                }
            }
        }    
    }
    return 0;
}

//UDSServer::UDSServer(const UDSServer& orig) {
//}

UDSServer::~UDSServer() {
}

