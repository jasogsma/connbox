/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TxLogEntry.h
 * Author: stephenaaskov
 *
 * Created on 30. marts 2018, 22:27
 */

#ifndef TXLOGENTRY_H
#define TXLOGENTRY_H

#include <string>

using namespace std;

class TxLogEntry {
public:
    TxLogEntry(char *line);
    TxLogEntry(char *parts[]);
    TxLogEntry(const TxLogEntry& orig);
    virtual ~TxLogEntry();
    string GetFileRef() const;
    string GetStatus() const;
    string GetDestination() const;
    string GetSize() const;
    string GetTime() const;
    string GetDate() const;
    string GetLes() const;
private:
    string m_les;
    string m_date;
    string m_time;
    string m_size;
    string m_destination;
    string m_status;
    string m_fileRef;    
};

#endif /* TXLOGENTRY_H */

