/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EGCListEntry.h
 * Author: stephenaaskov
 *
 * Created on 1. april 2018, 14:58
 */

#ifndef EGCLISTENTRY_H
#define EGCLISTENTRY_H

#include <string>
#include "json.hpp"

using nlohmann::json;

class EGCListEntry {
public:
    EGCListEntry(const char *p);
    EGCListEntry(const EGCListEntry& orig);
    virtual ~EGCListEntry();
    std::string GetDate() const;
    std::string GetTime() const;
    unsigned int GetSize() const;
    unsigned int GetNumber() const;
    
private:
    unsigned int m_number;
    unsigned int m_size;
    std::string m_time;
    std::string m_date;    
};


#endif /* EGCLISTENTRY_H */

