/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EGCListEntry.cpp
 * Author: stephenaaskov
 * 
 * Created on 1. april 2018, 14:58
 */

#include "EGCListEntry.h"
#include <cstring>
#include <iostream>

EGCListEntry::EGCListEntry(const char *p) {
    unsigned int number;
    unsigned int size;
    char time[6];
    char date[10];

    if (p != NULL) {
        int cnt = sscanf(p, "EGC %u %u %s %s", &number, &size, time, date);
        if (cnt == 4) {
            //std::cout << "Created EGC list entry with no. " << number << std::endl;
            m_number = number;
            m_size = size;
            m_time = std::string(time);
            m_date = std::string(date);
        }


    }

}

EGCListEntry::EGCListEntry(const EGCListEntry& orig) {
}

EGCListEntry::~EGCListEntry() {
}

std::string EGCListEntry::GetDate() const {
    return m_date;
}

std::string EGCListEntry::GetTime() const {
    return m_time;
}

unsigned int EGCListEntry::GetSize() const {
    return m_size;
}

unsigned int EGCListEntry::GetNumber() const {
    return m_number;
}


//Implement a to_json() !!