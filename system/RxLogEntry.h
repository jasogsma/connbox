/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RxLogEntry.h
 * Author: stephenaaskov
 * 
 */

#ifndef RXLOGENTRY_H
#define RXLOGENTRY_H

#include <string>

using namespace std;
/*
LES Sv P L       Time      Bytes  Mess.no S Status    File
-----------------------------------------------------------
312  0 0 7 11-03-21  11:10   336 612643   0 Mem      IN.072
*/

class RxLogEntry {
public:
   // RxLogEntry(char *line);
    RxLogEntry(char *parts[]);
    virtual ~RxLogEntry();
    string getLES();
    string getDate();
    string getTime();
    string getMessageNo();
    string getStatus();
    string getSize();
    string getFile();
private:
    string m_les;
    string m_date;
    string m_time;
    string m_size;
    string m_messageNo;
    string m_status;
    string m_file;
    
};

#endif /* RXLOGENTRY_H */

