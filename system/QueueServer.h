/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   QueueServer.h
 * Author: stephenaaskov
 *
 * Created on 11. februar 2018, 21:59
 */

#ifndef QUEUESERVER_H
#define QUEUESERVER_H

//#include <mqueue.h>
#include "../miniC/MiniC.h"
#include "json.hpp"
#include <string>
#include <memory>

using json = nlohmann::json;
using namespace std;

class QueueServer {
public:
    QueueServer();
    QueueServer(const QueueServer& orig);
    virtual ~QueueServer();
    
    bool open();
    void run();
    void setTerminal(MiniC *terminal);
    void handleMessage(json msg);
    bool sendJson(string recepient, json msg);
    bool sendText(string recepient, const char *text);
private:
   // mqd_t m_mqd;
 //   MiniC *m_terminal = NULL;
};

#endif /* QUEUESERVER_H */

