/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UDSServer.h
 * Author: stephenaaskov
 *
 * Created on 22. april 2018, 22:28
 */

#ifndef UDSSERVER_H
#define UDSSERVER_H
#include <string>
#include "json.hpp"

using namespace std;
using json = nlohmann::json;

class UDSServer {
public:
    UDSServer(string socketname);
   // UDSServer(const UDSServer& orig);
    virtual ~UDSServer();
    int start();
    int messageloop();
    void sendMessage(int socket, json msg);
    virtual void handleMessage(int socket, json msg);
private:
    string m_socketname;
    int m_serversocket;
};

#endif /* UDSSERVER_H */

