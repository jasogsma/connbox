/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TxLogEntry.cpp
 * Author: stephenaaskov
 * 
 * Created on 30. marts 2018, 22:27
 */

#include "TxLogEntry.h"
#include <string>
#include <cstring>
#include <iostream>

using namespace std;

/*
LES Sv P L       Time      Bytes Destination    MTCA Status  File/Ref
----------------------------------------------------------------------
112  0 1 0 13-10-24  10:59  1114 28             0610 Deliver 374353 
 */ 

TxLogEntry::TxLogEntry(char *line) {
    unsigned int fieldNum = 0;  
    char *p = strtok(line, " ");
    while (p) {
        switch(fieldNum) {
            case 9:
                m_status = string(p); // use smart pointer instead
                break;
            case 10:
                m_fileRef = string(p);
        }
        p = strtok(NULL," ");
        fieldNum++;
    }
}

TxLogEntry::TxLogEntry(char *parts[]) {    
    m_les = parts[0];
    m_date = parts[4];
    m_time = parts[5];
    m_size = parts[6];
    m_destination = parts[7];
    m_status = parts[9];
    m_fileRef = parts[10];
    std::cout << "destination:" << m_destination << std::endl;
}

TxLogEntry::TxLogEntry(const TxLogEntry& orig) {
}

TxLogEntry::~TxLogEntry() {
}

string TxLogEntry::GetFileRef() const {
    return m_fileRef;
}

string TxLogEntry::GetStatus() const {
    return m_status;
}

string TxLogEntry::GetDestination() const {
    return m_destination;
}

string TxLogEntry::GetSize() const {
    return m_size;
}

string TxLogEntry::GetTime() const {
    return m_time;
}

string TxLogEntry::GetDate() const {
    return m_date;
}

string TxLogEntry::GetLes() const {
    return m_les;
}

